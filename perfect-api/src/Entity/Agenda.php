<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Post;
use App\Repository\AgendaRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Types\UuidType;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Uid\Uuid;

#[ORM\Entity(repositoryClass: AgendaRepository::class)]
#[ApiResource(
    operations: [
        new Get(
            uriTemplate: '/agenda/{id}',
            normalizationContext: ['groups' => ['read:agendas'],],
            denormalizationContext: ['groups' => ['read:agendas'],],
            security: 'is_granted("ROLE_USER")',
        ),

        new GetCollection(
            uriTemplate: '/agendas',
            paginationEnabled: false,
            normalizationContext: ['groups' => ['read:agendas']],
            denormalizationContext: ['groups' => ['write:agendas']],
            security: 'is_granted("ROLE_USER")',
        ),

        new Post(
            uriTemplate: '/agenda',
            normalizationContext: ['groups' => ['read:agendas']],
            denormalizationContext: ['groups' => ['write:agendas']],
            security: 'is_granted("ROLE_USER")',
            read: false,
        ),
    ],
)]
class Agenda
{
    #[ORM\Id]
    #[ORM\Column(type: UuidType::NAME, unique: true)]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(class: 'doctrine.uuid_generator')]
    #[Groups(['read:agendas', 'read:agenda', 'write:Contact'])]
    private ?Uuid $id = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(['read:agendas', 'read:agenda', 'write:agendas'])]
    private ?string $title = null;

    #[ORM\ManyToOne(inversedBy: 'agendas')]
    private ?User $User = null;

    #[ORM\OneToMany(mappedBy: 'agenda', targetEntity: Contact::class, cascade: ['persist'])]
    #[Groups(['read:agendas', 'write:agendas', 'read:agenda'])]
    private Collection $contacts;

    public function __construct()
    {
        $this->contacts = new ArrayCollection();
    }

    public function getId(): ?Uuid
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->User;
    }

    public function setUser(?User $User): self
    {
        $this->User = $User;

        return $this;
    }

    /**
     * @return Collection<int, Contact>
     */
    public function getContacts(): Collection
    {
        return $this->contacts;
    }

    public function addContact(Contact $contact): self
    {
        if (!$this->contacts->contains($contact)) {
            $this->contacts->add($contact);
            $contact->setAgenda($this);
        }

        return $this;
    }

    public function removeContact(Contact $contact): self
    {
        if ($this->contacts->removeElement($contact)) {
            // set the owning side to null (unless already changed)
            if ($contact->getAgenda() === $this) {
                $contact->setAgenda(null);
            }
        }

        return $this;
    }
}
