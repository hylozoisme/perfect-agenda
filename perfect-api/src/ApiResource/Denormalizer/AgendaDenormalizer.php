<?php

namespace App\ApiResource\Denormalizer;

use App\Entity\Agenda;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\ContextAwareDenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareTrait;

class AgendaDenormalizer implements ContextAwareDenormalizerInterface, DenormalizerAwareInterface
{

    use DenormalizerAwareTrait;

    private Security $security;
    private const ALREADY_CALLED = 'AGENDA_DENORMALIZER_ALREADY_CALLED';

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    public function supportsDenormalization(mixed $data, string $type, string $format = null, array $context = []): bool
    {
        if (isset($context[self::ALREADY_CALLED])) {
            return false;
        }

        return $type === Agenda::class;
    }

    /**
     * @throws ExceptionInterface
     * @throws NonUniqueResultException
     * @throws NoResultException
     */
    public function denormalize(mixed $data, string $type, string $format = null, array $context = [])
    {
        $context[self::ALREADY_CALLED] = true;

        $agenda = $this->denormalizer->denormalize($data, $type, $format, $context);
        $agenda->setUser($this->security->getUser());

        return $agenda;
    }
}