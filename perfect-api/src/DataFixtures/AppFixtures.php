<?php

namespace App\DataFixtures;

use App\Entity\Agenda;
use App\Entity\Contact;
use App\Entity\User;
use App\Entity\UserProfil;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AppFixtures extends Fixture
{
    private UserPasswordHasherInterface $passwordHasher;

    public function __construct(UserPasswordHasherInterface $passwordHasher)
    {
        $this->passwordHasher = $passwordHasher;
    }

    public function load(ObjectManager $manager)
    {
        $user = new User();
        $user->setEmail('agenda@gmail.com');
        $user->setPassword($this->passwordHasher->hashPassword($user, 'password'));
        $user->setRoles(['ROLE_USER']);

        $userProfile = new UserProfil();
        $userProfile->setPrenom('Erwan');
        $userProfile->setNom('D');
        $userProfile->setUser($user);

        $agenda = new Agenda();
        $agenda->setTitle('Professionel');
        $agenda->setUser($user);

        $agenda2 = new Agenda();
        $agenda2->setTitle('Personnel');
        $agenda2->setUser($user);

        $agenda3 = new Agenda();
        $agenda3->setTitle('Famille');
        $agenda3->setUser($user);

        $manager->persist($user);
        $manager->persist($userProfile);
        $manager->persist($agenda);
        $manager->persist($agenda2);
        $manager->persist($agenda3);
        $manager->flush();


        for ($i = 0; $i <= 10; $i++) {
            $contact = new Contact();
            $contact->setNom($this->generateRandomLetters());
            $contact->setPrenom($this->generateRandomLetters());
            $contact->setPhone('0123456789');
            $contact->setAgenda($agenda);
            $manager->persist($contact);
        }

        for ($i = 0; $i <= 20; $i++) {
            $contact = new Contact();
            $contact->setNom($this->generateRandomLetters());
            $contact->setPrenom($this->generateRandomLetters());
            $contact->setPhone('0123456789');
            $contact->setAgenda($agenda2);
            $manager->persist($contact);
        }

        for ($i = 0; $i <= 19; $i++) {
            $contact = new Contact();
            $contact->setNom($this->generateRandomLetters());
            $contact->setPrenom($this->generateRandomLetters());
            $contact->setPhone('0123456789');
            $contact->setAgenda($agenda3);
            $manager->persist($contact);
        }

        $manager->flush();
    }

    private function generateRandomLetters($length = 5)
    {
        $letters = 'abcdefghijklmnopqrstuvwxyz';
        $result = '';

        for ($i = 0; $i < $length; $i++) {
            $result .= $letters[rand(0, 25)];
        }

        return ucfirst($result);
    }
}
