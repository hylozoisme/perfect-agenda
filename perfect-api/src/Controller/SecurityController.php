<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController{

        #[Route('/api/login', name: 'api_login', methods: ['POST'])]
        public function login(): \Symfony\Component\HttpFoundation\JsonResponse
        {
            $user = $this->getUser();
            return $this->json([
                'username' => $user->getUserIdentifier(),
                'roles' => $user->getRoles()
            ]);

        }


    #[Route('/connexion', name: 'app_security')]
    public function loginAdmin(AuthenticationUtils $authenticationUtils): Response
    {
        $error = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('@EasyAdmin/page/login.html.twig', [
            'error' => $error,
            'last_username' => $lastUsername,
            'translation_domain' => 'admin',
            'favicon_path' => '/favicon-admin.svg',
            'page_title' => 'FBI V3 - Connexion',
            'csrf_token_intention' => 'authenticate',
            'target_path' => $this->generateUrl('admin'),
            'username_label' => 'Nom d\'utilisateur',
            'password_label' => 'Mot de passe',
            'sign_in_label' => 'Connexion',

            'username_parameter' => '_username',
            'password_parameter' => '_password',

            'forgot_password_enabled' => true,

            'forgot_password_label' => 'Mot de passe oublié?',
            'remember_me_enabled' => true,
            'remember_me_parameter' => 'custom_remember_me_param',
            'remember_me_checked' => true,
            'remember_me_label' => 'Se souvenir',
        ]);
    }

    #[Route('/logout', name: 'app_logout')]
    public function logout()
    {
        // controller can be blank: it will never be called!
        //throw new \Exception('Don\'t forget to activate logout in security.yaml');
    }
}