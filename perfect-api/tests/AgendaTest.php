<?php

namespace App\Tests;

use App\Entity\Agenda;
use App\Entity\Contact;
use PHPUnit\Framework\TestCase;

class AgendaTest extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();
    }

    public function testSetTitle()
    {
        $agenda = new Agenda();
        $agenda->setTitle('Test Agenda');
        $this->assertSame('Test Agenda', $agenda->getTitle());
    }

    public function testCreateContact()
    {
        $agenda = new Agenda();
        $agenda->setTitle('Test Agenda');

        $contact = new Contact();
        $contact->setNom('Test');
        $contact->setPrenom('Contact');
        $contact->setAgenda($agenda);

        $this->assertSame('Test', $contact->getNom());
        $this->assertSame('Contact', $contact->getPrenom());
        $this->assertSame($agenda, $contact->getAgenda());
    }

    public function testDeleteContactFromAgenda()
    {
        $agenda = new Agenda('Test Agenda');
        $contact = new Contact('Test', 'Contact', $agenda);

        $agenda->removeContact($contact);
        $this->assertEmpty($agenda->getContacts());
    }

    protected function tearDown(): void
    {
        parent::tearDown();
    }
}
