<?php
namespace App\Tests;

use App\Entity\Contact;
use PHPUnit\Framework\TestCase;

class ContactTest extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();
    }

    public function testSetWebsite()
    {
        $contact = new Contact();
        $contact->setWebsite('https://example.com');
        $this->assertSame('https://example.com', $contact->getWebsite());

        $contact->setWebsite('not-a-valid-url');
        $this->assertNull($contact->getWebsite(), 'Expected website to be null for invalid URL');
    }

    public function testSetPhone()
    {
        $contact = new Contact();
        $contact->setPhone('0123456789');
        $this->assertSame('0123456789', $contact->getPhone());

        $contact->setPhone('not-a-valid-phone-number');
        $this->assertNull($contact->getPhone(), 'Expected phone number to be null for invalid format');
    }

    public function testSetEmail()
    {
        $contact = new Contact();
        $contact->setEmail('test@example.com');
        $this->assertSame('test@example.com', $contact->getEmail());

        $contact->setEmail('not-a-valid-email-address');
        $this->assertNull($contact->getEmail(), 'Expected email address to be null for invalid format');
    }

    public function testSetAddress()
    {
        $contact = new Contact();
        $contact->setAdress('24 Rue victor Schoelcher');
        $this->assertSame('24 Rue victor Schoelcher', $contact->getAdress());

        $contact->setAdress('not-a-valid-address');
        $this->assertNull($contact->getAdress(), 'Expected address to be null for invalid format');
    }

    protected function tearDown(): void
    {
        parent::tearDown();
    }
}
