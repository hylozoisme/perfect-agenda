#!/bin/bash

docker-compose up --build -d
echo "Waiting for the database to be ready..."
sleep 5
echo "Database is ready!"

cd perfect-api
echo "Installing dependencies..."
composer install
php bin/console doctrine:database:create --if-not-exists
yes | php bin/console make:migration
yes | php bin/console doctrine:migrations:migrate
yes | php bin/console doctrine:fixtures:load

php bin/phpunit
php -S localhost:8000 -t public/ &

cd ../perfect-front
npm install
echo "Node modules installed"
npm run start
