import {Component, OnInit, ViewChild} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {Router} from "@angular/router";
import {AuthService} from "../../core/authentification/auth.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  form!: FormGroup;
  @ViewChild('loginButton') loginButton!: any;

  constructor(private formBuilder: FormBuilder, private auth: AuthService, private router: Router) {
  }

  ngOnInit() {
    this.form = this.formBuilder.group({
      username: ['', [Validators.required]],
      password: ['', [Validators.required]]
    });
  }

  login() {
    this.loginButton.nativeElement.disabled = true;

    if (this.form.invalid) {
      this.loginButton.nativeElement.disabled = false;
    } else {
      this.auth.signIn(this.form.get('username')?.value, this.form.get('password')?.value);
      this.loginButton.nativeElement.disabled = false;
    }
  }
}
