import { Component } from '@angular/core';
import {Contact} from "../../shared/models/contact";
import {AgendaService} from "../../core/services/agenda.service";
import {ActivatedRoute} from "@angular/router";
import {Agenda} from "../../shared/models/agenda";

@Component({
  selector: 'app-agenda',
  templateUrl: './agenda.component.html',
  styleUrls: ['./agenda.component.scss']
})
export class AgendaComponent {

  uuid: string = '';
  agenda!: Agenda;

  constructor(private agendaService: AgendaService, private activatedRoute: ActivatedRoute) {
    this.activatedRoute.params.subscribe(params => {
      this.uuid = params['id'];
      console.log(this.uuid);

      this.agendaService.getAgenda(this.uuid).subscribe((agenda) => {
        this.agenda = agenda as Agenda;
      })
    })
  }

  openModal() {
    this.agendaService.openModal();
    console.log('openModal');
  }
}
