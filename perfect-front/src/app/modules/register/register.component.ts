import {Component, OnInit, ViewChild} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {Router} from "@angular/router";
import {AuthService} from "../../core/authentification/auth.service";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  form!: FormGroup;
  @ViewChild('loginButton') loginButton!: any;

  constructor(private formBuilder: FormBuilder, private auth: AuthService, private router: Router) {
  }

  ngOnInit() {
    this.form = this.formBuilder.group({
      username: ['', [Validators.required]],
      password: ['', [Validators.required]],
      confirmPassword: ['', [Validators.required]]
    });
  }

  register() {
    this.loginButton.nativeElement.disabled = true;

    if (this.form.invalid || this.form.get('password')?.value != this.form.get('confirmPassword')?.value) {
      this.loginButton.nativeElement.disabled = false;
      return;
    } else {
      // this.auth.register(this.form.get('username')?.value, this.form.get('password')?.value)
      //   .subscribe((response) => {
      //     this.router.navigate(['/connexion']);
      //   }, (error) => {
      //     this.loginButton.nativeElement.disabled = false;
      //   });
      return;
    }
  }
}
