import { Component } from '@angular/core';
import { Agenda } from "../../shared/models/agenda";
import {AgendaService} from "../../core/services/agenda.service";
import {Observable} from "rxjs";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent {

  agendaList: Agenda[] = []
  constructor(private agendaService: AgendaService) {
    this.agendaService.getAgendas().subscribe((agendas) => {
      this.agendaList = agendas as Agenda[];
    })
  }
}
