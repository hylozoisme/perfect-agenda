import { Injectable } from '@angular/core';
import {Observable, Subject} from "rxjs";
import {Agenda} from "../../shared/models/agenda";
import {HttpClient} from "@angular/common/http";
import {api} from "../api";

@Injectable({
  providedIn: 'root'
})
export class AgendaService {

  agenda: Agenda[] = [];

  private articleClicked = new Subject<number>();
  articleClicked$ = this.articleClicked.asObservable();

  private modalOpen = false;
  private modalOpenSubject = new Subject<boolean>();
  modalOpen$ = this.modalOpenSubject.asObservable();

  constructor(private http: HttpClient) { }

  openModal() {
    this.modalOpen = true;
    this.modalOpenSubject.next(true);
    console.log('openModal');
  }

  closeModal() {
    this.modalOpen = false;
    this.modalOpenSubject.next(false);
  }

  isModalOpen(): Observable<boolean> {
    return this.modalOpenSubject.asObservable();
  }

  getAgendas() {
    return this.http.get(api + '/agendas');
  }

  getAgenda(id: string) {
    return this.http.get(api + '/agenda/' + id);
  }

  postAgenda(agenda: Agenda) {
    return this.http.post(api + '/agenda', agenda);
  }

  postContact(contact: any) {
    return this.http.post(api + '/contact', contact);
  }
}
