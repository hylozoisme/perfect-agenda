import { Component, HostListener, OnInit } from '@angular/core';
import {AuthService} from "../authentification/auth.service";
import { LoggedUser } from "../../shared/models/loggedUser";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent {
  isScroll: boolean = false;
  menuOpen: boolean = false;
  loggedUser!: LoggedUser;
  initials: string = '';
  @HostListener('window:scroll', [])
  onWindowScroll() {
    const scrollPosition = window.scrollY || document.documentElement.scrollTop || document.body.scrollTop || 0;
    this.isScroll = scrollPosition >= 20;
  }

  constructor(private authService: AuthService) {
  }

  ngOnInit(): void {
    this.authService.getUserProfile().subscribe((res: any) => {
      this.loggedUser = res;
      this.initials = this.loggedUser.userProfil.prenom.charAt(0) + this.loggedUser.userProfil.nom.charAt(0);
    })
  }

  logout(): void {
    this.authService.doLogout();
  }

  toggleMenu() {
    this.menuOpen = !this.menuOpen;
  }
}
