import { Injectable } from '@angular/core';
import { User } from "../../shared/models/user";
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import {
  HttpClient,
  HttpHeaders,
  HttpErrorResponse,
} from '@angular/common/http';
import { Router } from '@angular/router';
import jwt_decode from "jwt-decode";
import { api } from "../api";

interface JwtPayload {
  exp: number;
}


@Injectable({
  providedIn: 'root',
})

export class AuthService {
  endpoint: string = api;
  headers = new HttpHeaders().set('Content-Type', 'application/json');
  constructor(private http: HttpClient, public router: Router) { }

  // Sign-up
  signUp(user: User): Observable<any> {
    let api = `${this.endpoint}/register-user`;
    return this.http.post(api, user).pipe(catchError(this.handleError));
  }

  // Sign-in
  signIn(username: string, password: string) {
    return this.http
      .post<any>(`${this.endpoint}/login`, {"username": username, "password": password})
      .subscribe((res: any) => {
        localStorage.setItem('access_token', res.token);
        localStorage.setItem('refresh_token', res.refresh_token);
        this.router.navigate(['/']);
      });
  }

  validateToken(token: string): boolean {
    try {
      const decoded: JwtPayload = jwt_decode(token);
      const currentTime = Date.now() / 1000;
      if (decoded && decoded.exp && decoded.exp > currentTime) {
        return true;
      }
    } catch (error) {
      console.error(error);
    }
    return false;
  }

  refreshToken(){
    return this.http
      .post<any>(`${this.endpoint}/token/refresh`, {"refresh_token": this.getRefreshToken()})
  }

  getToken() {
    return localStorage.getItem('access_token');
  }

  getRefreshToken(){
    return localStorage.getItem('refresh_token');
  }

  get isLoggedIn(): boolean {
    let authToken = localStorage.getItem('access_token');
    return authToken !== null;
  }

  doLogout() {
    let removeToken = localStorage.removeItem('access_token');
    if (removeToken == null) {
      this.router.navigate(['connexion']);
    }
  }

  getUserProfile() {
    let api = `${this.endpoint}/user/profil`;
    return this.http.get(api, { headers: this.headers }).pipe(
      map((res: any) => {
        return res || {}
      })
    );
  }

  handleError(error: HttpErrorResponse) {
    let msg = '';
    if (error.error instanceof ErrorEvent) {
      // client-side error
      msg = error.error.message;
    } else {
      // server-side error
      msg = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    return throwError(msg);
  }
}
