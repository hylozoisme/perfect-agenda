import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {HomeComponent} from "./modules/home/home.component";
import {LoginComponent} from "./modules/login/login.component";
import {RegisterComponent} from "./modules/register/register.component";
import {AuthGuard} from "./core/authentification/auth.guard";
import {AgendaComponent} from "./modules/agenda/agenda.component";


const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'agenda/:id',
    component: AgendaComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'connexion',
    component: LoginComponent,
  },
  {
    path: 'inscription',
    component: RegisterComponent
  },
  {
    //404
    path: '**',
    redirectTo: ''
  },
  {
    //500
    path: 'error',
    redirectTo: ''
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { scrollPositionRestoration: 'enabled' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
