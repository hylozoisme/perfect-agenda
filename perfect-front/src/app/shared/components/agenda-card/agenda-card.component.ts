import {Component, Input} from '@angular/core';
import { Agenda } from "../../models/agenda";

@Component({
  selector: 'app-agenda-card',
  templateUrl: './agenda-card.component.html',
  styleUrls: ['./agenda-card.component.scss']
})
export class AgendaCardComponent {
  @Input('agenda') agenda: Agenda | undefined;
  nbContacts: number = 0;

  constructor() { }

  makeInitials(nom: string, prenom: string) {
    return nom.charAt(0) + prenom.charAt(0);
  }

  ngOnChanges() {
    if (this.agenda) {
      if(this.agenda.contacts.length > 7){
        this.nbContacts = this.agenda.contacts.length - 7;
      }
      else if(this.agenda.contacts.length > 99){
        this.nbContacts = 99;
      }
      else {
        this.nbContacts = this.agenda.contacts.length;
      }
    }
  }
}
