import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AgendaContactComponent } from './agenda-contact.component';

describe('AgendaContactComponent', () => {
  let component: AgendaContactComponent;
  let fixture: ComponentFixture<AgendaContactComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AgendaContactComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AgendaContactComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
