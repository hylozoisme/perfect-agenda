import {Component, Input} from '@angular/core';
import {Contact} from "../../models/contact";

@Component({
  selector: 'app-agenda-contact',
  templateUrl: './agenda-contact.component.html',
  styleUrls: ['./agenda-contact.component.scss']
})
export class AgendaContactComponent {
  @Input() contact: Contact | undefined;
}
