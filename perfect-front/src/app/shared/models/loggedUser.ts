import { UserProfil } from "./userProfil";

export interface LoggedUser {
  id: number;
  email: string;
  roles: string[];
  userProfil: UserProfil;
}
