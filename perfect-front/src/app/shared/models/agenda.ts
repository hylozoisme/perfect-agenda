import {Contact} from "./contact";

export interface Agenda {
  id: string;
  title: string;
  contacts: Contact[];
}
