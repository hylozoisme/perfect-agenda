# The (not so) perfect Agenda

## Versions
- Composer 2.5.1
- Php 8.2
- Node 19.4
- Npm 9.2

## How to run ?
```
sh run.sh
```

## Liens

- Client : http://localhost:4200 (Log: agenda@gmail.com / password)
- Server : http://localhost:8000
- Swagger UI: http://localhost:8000/api/docs
- PhpMyAdmin : http://localhost:8080 (Log: root / root)

Cet agenda fonctionne avec un back-end Symfony, et un front qui utilise Angular.

## Fonctionnalités implémentées
- [ ] Inscription
- [x] Connexion JWT
- [x] Refresh JWT
- [ ] Création d'un contact
- [x] Lister les agendas
- [x] Lister les contacts d'un agenda
- [x] Ajouter un contact dans un agenda

## Diagramme de classe
[![](https://mermaid.ink/img/pako:eNqNU01vwjAM_StRTkECfkAPSIxx4DAJbUy79JIlbonWJlWSDk2I_758uC2wHeihdW0_5z3bOVNhJNCCioY796x4bXlbaqksCK-MJk-HUpc6Rcm6Bi05OZeahEfJ_PXKN5BNYbTnwrv89-7AJsuBP8QkllJnJDkttOYbNhnBEImxAIhgFl_RdRkYYPo9BW3abHQWRpsnttmGlqsmmyf4dMoj4e5oNJpcWnDIfCCxjTCWwBOzfcSwhJycuTUsnzm5P_JhDA-dkSxmkLMPr5Oxct37I2ivBPcgo-id9mArLgCV1uFYzGWzocZQJQKmjiD7a8nWNIDKOqxy3SIM9aHM3poqoq4m9J-yehDsGHq4lH8SL7cU7zWB5Q42FmRUzpuxVJ2Hv0v-SoU1mAKvUQn7p3gmPnQhDMebB1akTxuKzUxtJMvlijwyl5v8-xBelMViNWwspkfPGvdydEz86Zy2YMPgZLiRSUtJA4MWSloEU3L7VdJAOOTx3pu3Hy1o4W0Pc9p3MpDEC0yLKjR09G6l8saOTki_L3jv4-fyC5LPXAk?type=png)](https://mermaid.live/edit#pako:eNqNU01vwjAM_StRTkECfkAPSIxx4DAJbUy79JIlbonWJlWSDk2I_758uC2wHeihdW0_5z3bOVNhJNCCioY796x4bXlbaqksCK-MJk-HUpc6Rcm6Bi05OZeahEfJ_PXKN5BNYbTnwrv89-7AJsuBP8QkllJnJDkttOYbNhnBEImxAIhgFl_RdRkYYPo9BW3abHQWRpsnttmGlqsmmyf4dMoj4e5oNJpcWnDIfCCxjTCWwBOzfcSwhJycuTUsnzm5P_JhDA-dkSxmkLMPr5Oxct37I2ivBPcgo-id9mArLgCV1uFYzGWzocZQJQKmjiD7a8nWNIDKOqxy3SIM9aHM3poqoq4m9J-yehDsGHq4lH8SL7cU7zWB5Q42FmRUzpuxVJ2Hv0v-SoU1mAKvUQn7p3gmPnQhDMebB1akTxuKzUxtJMvlijwyl5v8-xBelMViNWwspkfPGvdydEz86Zy2YMPgZLiRSUtJA4MWSloEU3L7VdJAOOTx3pu3Hy1o4W0Pc9p3MpDEC0yLKjR09G6l8saOTki_L3jv4-fyC5LPXAk)